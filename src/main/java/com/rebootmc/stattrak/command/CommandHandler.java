/*
 * Copyright (C) SainttX <http://sainttx.com>
 * Copyright (C) contributors
 *
 * This file is part of Auctions.
 *
 * Auctions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Auctions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Auctions.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.rebootmc.stattrak.command;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.subcommand.*;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.HashSet;
import java.util.Set;

/**
 * Handles command distribution for the auction plugin
 */
public class CommandHandler implements CommandExecutor {

    /*
     * All commands for the plugin
     */
    private static Set<SubCommand> commands = new HashSet<SubCommand>();

    private StatTrakPlugin plugin;

    /**
     * Constructor. Initializes all subcommands.
     */
    public CommandHandler(StatTrakPlugin plugin) {
        this.plugin = plugin;
        addCommand(new AddCommand(plugin));
        addCommand(new AddRenamesCommand(plugin));
        addCommand(new CreditsCommand(plugin));
        addCommand(new RedeemCommand(plugin));
        addCommand(new RemoveCommand(plugin));
        addCommand(new RemoveRenamesCommand(plugin));
        addCommand(new RenameCommand(plugin));
    }

    /**
     * Registers a sub command
     *
     * @param command the command
     */
    public static void addCommand(SubCommand command) {
        commands.add(command);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            sendMenu(sender);
        } else {
            String sub = args[0];

            for (SubCommand cmd : commands) {
                if (cmd.canTrigger(sub)) {
                    if (!sender.hasPermission(cmd.getPermission())) {
                        sender.sendMessage(plugin.getMessage("messages.permission"));
                    } else {
                        cmd.onCommand(sender, command, label, args);
                    }
                    return true;
                }
            }

            sendMenu(sender);
        }
        return true;
    }

    /**
     * Sends the auction menu to a sender
     *
     * @param sender The sender to send the menu too
     */
    public void sendMenu(CommandSender sender) {
        for (String message : plugin.getConfig().getStringList("messages.helpMenu")) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
        }
    }
}
