package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Matthew on 22/05/2015.
 */
public class RedeemCommand extends SubCommand {

    public RedeemCommand(StatTrakPlugin plugin) {
        super(plugin, "stattrak.redeem", "redeem");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only players can redeem StatTrak coins on their items");
        } else {
            Player player = (Player) sender;
            int coins = plugin.getCoinsFile().getCoins(player);

            if (coins <= 0) {
                sender.sendMessage(plugin.getMessage("messages.notEnoughCoins"));
            } else {
                ItemStack item = player.getItemInHand();

                if (!plugin.canBeTracked(item)) {
                    sender.sendMessage(plugin.getMessage("messages.cannotBeTracked"));
                } else if (plugin.isStatTracked(item)) {
                    sender.sendMessage(plugin.getMessage("messages.alreadyBeingTracked"));
                } else {
                    player.setItemInHand(plugin.setupItem(item, player));
                    plugin.getCoinsFile().setCoins(player, coins - 1);
                    plugin.getCoinsFile().saveConfiguration();
                    sender.sendMessage(plugin.getMessage("messages.success"));
                }
            }
        }
        return true;
    }
}
