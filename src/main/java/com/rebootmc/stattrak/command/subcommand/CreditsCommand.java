package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matthew on 22/05/2015.
 */
public class CreditsCommand extends SubCommand {

    public CreditsCommand(StatTrakPlugin plugin) {
        super(plugin, "stattrak.credits", "credits", "credit", "cr", "c");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 2 && !(sender instanceof Player)) {
            sender.sendMessage(plugin.getMessage("messages.usageCredits"));
        } else {
            Player target = args.length < 2 ? (Player) sender : Bukkit.getPlayer(args[1]);

            if (target != sender && !sender.hasPermission("stattrak.credits.others")) {
                sender.sendMessage("Can't check the coins of other players");
            } else if (target == null) {
                sender.sendMessage(plugin.getMessage("messages.targetNotFound"));
            } else {
                int coins = plugin.getCoinsFile().getCoins(target);
                int renames = plugin.getRenameFile().getRedeems(target);
                sender.sendMessage(plugin.getMessage("messages.credits")
                        .replace("[player]", target.getName())
                        .replace("[redeems]", Integer.toString(coins))
                        .replace("[renames]", Integer.toString(renames)));
            }
        }
        return true;
    }
}
