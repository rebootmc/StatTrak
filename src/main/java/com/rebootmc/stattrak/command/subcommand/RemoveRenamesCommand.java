package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matthew on 23/05/2015.
 */
public class RemoveRenamesCommand extends SubCommand {

    public RemoveRenamesCommand(StatTrakPlugin plugin) {
        super(plugin, "stattrak.removerenames", "removerenames");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(plugin.getMessage("messages.usageRemoveRenames"));
        } else {
            Player target = Bukkit.getPlayer(args[1]);

            if (target == null) {
                sender.sendMessage(plugin.getMessage("messages.targetNotFound"));
            } else {
                int renames;

                try {
                    renames = Integer.parseInt(args[2]);
                } catch (NumberFormatException ex) {
                    sender.sendMessage(plugin.getMessage("messages.invalidNumber"));
                    return true;
                }

                if (renames < 0) {
                    sender.sendMessage(plugin.getMessage("messages.invalidNumber"));
                } else {
                    int current = plugin.getRenameFile().getRedeems(target);
                    plugin.getRenameFile().setRedeems(target, Math.max(0, current - renames));
                    plugin.getRenameFile().saveConfiguration();
                    sender.sendMessage(plugin.getMessage("messages.takeRenames")
                            .replace("[amount]", Integer.toString(renames)).replace("[player]", target.getName()));
                    target.sendMessage(plugin.getMessage("messages.lostRenames")
                            .replace("[amount]", Integer.toString(renames)).replace("[player]", sender.getName()));
                }
            }
        }
        return true;
    }
}