package com.rebootmc.stattrak.storage;


import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

/**
 * Created by Matthew on 02/02/2015.
 */
public class CoinsFile extends Configuration {

    /**
     * Creates the token file
     *
     * @param plugin The TokenFile plugin
     * @param fileName The name of the file
     */
    public CoinsFile(JavaPlugin plugin, String fileName) {
        super(plugin, fileName);
    }

    /**
     * Gets a players token amount
     *
     * @param player The player
     *
     * @return The amount of coins the player currently has
     */
    public int getCoins(Player player) {
        return getCoins(player.getUniqueId());
    }

    /**
     * Gets a players token amount
     *
     * @param uuid The ID of the player
     *
     * @return The amount of coins the player currently has
     */
    public int getCoins(UUID uuid) {
        return getInt(uuid.toString());
    }

    /**
     * Sets the amount of coins a player has
     *
     * @param player The player
     * @param amount The new amount of coins the player should have
     *
     * @return The amount of coins the player currently has
     */
    public int setCoins(Player player, int amount) {
        return setCoins(player.getUniqueId(), amount);
    }

    /**
     * Sets the amount of coins a player has
     *
     * @param uuid The ID of the player
     * @param amount The new amount of coins the player should have
     *
     * @return The amount of coins the player currently has
     */
    public int setCoins(UUID uuid, int amount) {
        set(uuid.toString(), amount);
        return getCoins(uuid);
    }
}
