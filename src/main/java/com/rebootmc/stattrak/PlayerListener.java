package com.rebootmc.stattrak;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Handles various events surrounding player StatTrak items
 */
public class PlayerListener implements Listener {

    private StatTrakPlugin plugin;

    public PlayerListener(StatTrakPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player dead = event.getEntity();
        Player killer = null;
        EntityDamageEvent lastCause = dead.getLastDamageCause();

        if (lastCause instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent edbe = (EntityDamageByEntityEvent) lastCause;
            Entity damager = edbe.getDamager();

            if (damager instanceof Player) {
                killer = (Player) damager;
            } else if (damager instanceof Projectile) {
                Projectile projectile = (Projectile) damager;
                ProjectileSource shooter = projectile.getShooter();

                if (shooter instanceof Player) {
                    killer = (Player) shooter;
                }
            }
        }

        if (killer != null) {
            ItemStack hand = killer.getItemInHand();

            if (hand != null && plugin.isStatTracked(hand)) {
                killer.setItemInHand(plugin.addKill(hand));
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getInventory().getType() == InventoryType.ANVIL) {
            Inventory clicked = event.getClickedInventory();
            InventoryView view = event.getView();

            if (event.getClick().isShiftClick() && view.getBottomInventory() == clicked) {
                ItemStack itm = event.getCurrentItem();

                if (itm != null && plugin.isStatTracked(itm)) {
                    event.setCancelled(true);
                }
            } else if (clicked == view.getTopInventory()) {
                ItemStack itm = event.getCursor();

                if (itm != null && plugin.isStatTracked(itm)) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDeathSaveItems(PlayerDeathEvent event) {
        final Player dead = event.getEntity();
        Iterator<ItemStack> drops = event.getDrops().iterator();
        final List<ItemStack> ret = new ArrayList<ItemStack>();

        while (drops.hasNext()) {
            ItemStack itm = drops.next();
            if (plugin.isStatTracked(itm)) {
                drops.remove();
                ret.add(itm);
            }
        }

        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                for (ItemStack itm : ret) {
                    dead.getInventory().addItem(itm);
                }
            }
        }, 1L);
    }
}
