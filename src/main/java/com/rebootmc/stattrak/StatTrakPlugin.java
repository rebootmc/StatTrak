package com.rebootmc.stattrak;

import com.rebootmc.stattrak.command.CommandHandler;
import com.rebootmc.stattrak.storage.CoinsFile;
import com.rebootmc.stattrak.storage.RenameFile;
import com.taiter.ce.Tools;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Matthew on 22/05/2015.
 */
public class StatTrakPlugin extends JavaPlugin {

    private CoinsFile file;
    private RenameFile redeems;
    private Pattern KILLS_REGEX = null;
    private Pattern DISPLAY_NAME_REGEX;
    private String displayNameText, killsLoreText;
    private List<String> baseLore = new ArrayList<String>();

    private File savedFile;
    private YamlConfiguration savedYaml;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        build();
        this.file = new CoinsFile(this, "coins.yml");
        this.redeems = new RenameFile(this, "redeems.yml");
        this.savedFile = new File(getDataFolder(), "saved.yml");

        if (!savedFile.exists()) {
            try {
                savedFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.savedYaml = YamlConfiguration.loadConfiguration(savedFile);

        // register commands and listeners
        getCommand("stattrak").setExecutor(new CommandHandler(this));
        getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
        getServer().getPluginManager().registerEvents(new CombatTagPlusListener(this), this);
    }

    @Override
    public void onDisable() {
        try {
            savedYaml.save(savedFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves an item for a player
     * @param player the player
     */
    public void saveItem(Player player, ItemStack item) {
        List<ItemStack> items = getItems(player);
        items.add(item);
        savedYaml.set(player.getUniqueId().toString(), items);
    }

    /**
     * Removed a players saved items
     *
     * @param player the player
     */
    public void clearSaved(Player player) {
        savedYaml.set(player.getUniqueId().toString(), null);
    }

    /**
     * Gets a players saved items
     *
     * @param player the player
     * @return all saved items for the player
     */
    public List<ItemStack> getItems(Player player) {
        String id = player.getUniqueId().toString();
        if (!savedYaml.isList(id)) {
            return new ArrayList<ItemStack>();
        }

        return (List<ItemStack>) savedYaml.getList(id);
    }

    /*
     * Builds our regex from config
     */
    private void build() {
        getLogger().info("Building all name and lore patterns");
        this.displayNameText = getConfig().getString("settings.stattrak.name");
        List<String> base = getConfig().getStringList("settings.stattrak.lore");

        displayNameText = ChatColor.translateAlternateColorCodes('&', displayNameText);
        DISPLAY_NAME_REGEX = Pattern.compile(displayNameText.replace("[custom]", "([a-zA-Z0-9 ]+)"));
        for (String str : base) {
            this.baseLore.add(ChatColor.translateAlternateColorCodes('&', str));

            if (str.contains("[kills]")) {
                this.killsLoreText = ChatColor.translateAlternateColorCodes('&', str);
                KILLS_REGEX = Pattern.compile(killsLoreText.replace("[kills]", "([0-9]+)"));
            }
        }
    }

    /**
     * Gets the coins file
     *
     * @return the coins file
     */
    public CoinsFile getCoinsFile() {
        return file;
    }

    /**
     * Gets the redeems file
     *
     * @return the redeems file
     */
    public RenameFile getRenameFile() {
        return redeems;
    }

    /**
     * Gets if an item can be stat tracked
     *
     * @param item the item
     * @return true if the item can be tracked
     */
    public boolean canBeTracked(ItemStack item) {
        if (item == null) {
            return false;
        }

        if (item.hasItemMeta()) {
            if (item.getItemMeta().hasLore()
                    && Tools.checkForEnchantments(item.getItemMeta().getLore())) {
                return false;
            } else if (item.getItemMeta().hasDisplayName()
                    && Tools.getEnchantmentByDisplayname(item.getItemMeta().getDisplayName()) != null) {
                return false;
            }
        }

        Material type = item.getType();
        return getConfig().isList("settings.trackableMaterials")
                && getConfig().getStringList("settings.trackableMaterials").contains(type.toString());
    }

    /**
     * Gets if an item has StatTrak applied
     *
     * @param item the item
     * @return true if the item is being tracked
     */
    public boolean isStatTracked(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        if (meta != null && meta.hasDisplayName()) {
            Matcher matcher = DISPLAY_NAME_REGEX.matcher(meta.getDisplayName());
            return matcher.matches();
        }
        return false;
    }

    /**
     * Sets up StatTrak on an item
     *
     * @param item  the item
     * @param owner the owner of the item
     */
    public ItemStack setupItem(ItemStack item, Player owner) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayNameText.replace("[custom]", "Custom Text"));

        List<String> lore = new ArrayList<String>();
        for (String str : baseLore) {
            lore.add(str.replace("[kills]", "0").replace("[player]", owner.getName()));
        }
        meta.setLore(lore);
        meta.spigot().setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }

    /**
     * Changes an items name
     *
     * @param item the item
     * @param name the new name
     * @return the changed item
     */
    public ItemStack changeName(ItemStack item, String name) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayNameText.replace("[custom]", name));
        item.setItemMeta(meta);
        return item;
    }

    /**
     * Adds a kill to a StatTrak'd item
     *
     * @param item the item
     * @return the changed item
     */
    public ItemStack addKill(ItemStack item) {
        if (KILLS_REGEX == null) {
            return item;
        }
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();
        List<String> parsed = new ArrayList<String>();
        for (String str : lore) {
            Matcher matcher = KILLS_REGEX.matcher(str);
            if (matcher.find()) {
                int kills = Integer.parseInt(matcher.group(1));
                parsed.add(killsLoreText.replace("[kills]", Integer.toString(kills + 1)));
            } else {
                parsed.add(str);
            }
        }
        meta.setLore(parsed);
        item.setItemMeta(meta);
        return item;
    }

    /**
     * Returns a message from a path within configuration
     *
     * @param path the path in config
     * @return the message at the path
     */
    public String getMessage(String path) {
        return ChatColor.translateAlternateColorCodes('&', getConfig().getString(path));
    }
}
