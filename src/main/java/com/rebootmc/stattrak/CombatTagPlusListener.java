package com.rebootmc.stattrak;

import net.minelink.ctplus.CombatTagPlus;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by Matthew on 8/30/2015.
 */
public class CombatTagPlusListener implements Listener {

    private StatTrakPlugin plugin;

    public CombatTagPlusListener(StatTrakPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Inventory inv = player.getInventory();
        CombatTagPlus ctplus = (CombatTagPlus) Bukkit.getPluginManager().getPlugin("CombatTagPlus");

        // Save players items if tagged
        if (ctplus.getTagManager().isTagged(player.getUniqueId())) {
            for (ItemStack itm : inv) {
                if (itm != null && plugin.isStatTracked(itm)) {
                    inv.removeItem(itm);
                    plugin.saveItem(player, itm);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        List<ItemStack> saved = plugin.getItems(player);

        for (ItemStack itm : saved) {
            player.getInventory().addItem(itm);
        }

        plugin.clearSaved(player);
    }
}
